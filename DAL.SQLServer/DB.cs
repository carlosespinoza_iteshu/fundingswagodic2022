﻿using COMMON.Interfaces;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.SQLServer
{
    public class DB : IDB
    {
        SqlConnection connection;
        string cadena = "";
        public DB()
        {
            cadena = System.Configuration.ConfigurationManager.AppSettings["Cadena"];
            connection = new SqlConnection("Data Source=localhost;Initial Catalog=MiTiendita;Persist Security Info=True;User ID=sa;Password=Luxandra0;TrustServerCertificate=True");
        }
        public int Comando(string sql)
        {
            SqlCommand comando = new SqlCommand(sql, connection);
            return comando.ExecuteNonQuery();
        }

        public bool Conectar()
        {
            try
            {
                connection.Open();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public object Consulta(string sql)
        {
            SqlCommand command = new SqlCommand(sql, connection);
            return command.ExecuteReader();
        }

        public bool Desconectar()
        {
            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Close();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }
    }
}
