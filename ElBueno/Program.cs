﻿using COMMON.Interfaces;
using DAL.MySQL;
using DAL.SQLServer;

IDB db;

Console.Write("Conectarse a MySQL (S/N): ");
string opcion=Console.ReadLine();

if (opcion == "S")
{
    db = new DAL.MySQL.DB();
}
else
{
    db = new DAL.SQLServer.DB();
}

db.Conectar();

