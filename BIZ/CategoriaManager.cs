﻿using COMMON.Entidades;
using COMMON.Interfaces;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ
{
    public class CategoriaManager
    {
        //CRUD
        IDB db;

        public string Error { get; private set; }
        public CategoriaManager(IDB db)
        {
            this.db = db;
            db.Conectar();
        }
        ~CategoriaManager()
        {
            db.Desconectar();
        }

        public Categoria Insertar(Categoria categoria)
        {
            try
            {
                if(db.Comando($"Insert into Categorias(Nombre) values('{categoria.Nombre}');") == 1)
                {
                    Error = "";
                    return UltimaCategoriaInsertada();
                }
                else
                {
                    Error = "No se pudo insertar la categoria";
                    return null;
                }

            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        private Categoria UltimaCategoriaInsertada()
        {
            
            SqlDataReader r =(SqlDataReader) db.Consulta("select top(1) * from Categorias order by IdCategoria Desc;");
            Categoria c = new Categoria();
            r.Read();
            c.IdCategoria = r.GetInt32(0);
            c.Nombre= r.GetString(1);
            r.Close();
            return c;
        }

        public List<Categoria> ObtenerTodas
        {
            get
            {
                try
                {
                    SqlDataReader r = (SqlDataReader)db.Consulta("select * from Categorias;");
                    List<Categoria> categorias=new List<Categoria>();
                    while (r.Read())
                    {
                        categorias.Add(new Categoria()
                        {
                            IdCategoria = r.GetInt32(0),
                            Nombre = r.GetString(1)
                        });
                    }
                    r.Close();
                    Error = "";
                    return categorias;
                
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
        }

        public Categoria Actualizar(Categoria categoria)
        {
            try
            {
                int r = db.Comando($"Update Categorias SET Nombre='{categoria.Nombre}' where IdCategoria={categoria.IdCategoria}");
                if (r == 1)
                {
                    Error = "";
                    return BuscarPorId(categoria.IdCategoria);
                }
                else
                {
                    Error = "No se pudo actualizar la categoria";
                    return null;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public bool Eliminar(int idCategoria)
        {
            try
            {
                int r = db.Comando($"Delete from Categorias where IdCategoria={idCategoria}");
                if (r == 1)
                {
                    Error = "";
                    return true;
                }
                else
                {
                    Error = "No se pudo eliminar la categoría";
                    return false;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public Categoria BuscarPorId(int idCategoria)
        {
            try
            {
                SqlDataReader r = (SqlDataReader)db.Consulta($"select * from Categorias where idCategoria={idCategoria};");
                List<Categoria> categorias = new List<Categoria>();
                while (r.Read())
                {
                    categorias.Add(new Categoria()
                    {
                        IdCategoria = r.GetInt32(0),
                        Nombre = r.GetString(1)
                    });
                }
                r.Close();
                Error = "";
                return categorias.SingleOrDefault();

            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public List<Categoria> BuscarCategoria(string texto)
        {
            try
            {
                SqlDataReader r = (SqlDataReader)db.Consulta($"select * from Categorias where Nombre like '%{texto}%';");
                List<Categoria> categorias = new List<Categoria>();
                while (r.Read())
                {
                    categorias.Add(new Categoria()
                    {
                        IdCategoria = r.GetInt32(0),
                        Nombre = r.GetString(1)
                    });
                }
                r.Close();
                Error = "";
                return categorias;

            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
    }
}
