﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Interfaces
{
    /// <summary>
    /// Esta clase permite conectarse a una BD y hacer operaciones sobre ella.
    /// </summary>
    public interface IDB
    {
        

        /// <summary>
        /// Este método conecta con la BD
        /// </summary>
        /// <returns>Indica si la conexión fue exitosa</returns>
        bool Conectar();

        object Consulta(string sql);
        /// <summary>
        /// Ejecuta un comando SQL sobre la BD
        /// </summary>
        /// <param name="sql">Instrucción SQL a ejecutar p.e. Insert, Update, Delete</param>
        /// <returns>Número de filas afectadas</returns>
        int Comando(string sql);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool Desconectar();
    }
}
