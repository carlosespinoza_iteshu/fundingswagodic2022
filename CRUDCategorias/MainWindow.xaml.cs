﻿using BIZ;
using COMMON.Entidades;
using DAL.SQLServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CRUDCategorias
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CategoriaManager manager;
        Categoria categoria;
        bool esEdicion;
        public MainWindow()
        {
            InitializeComponent();
            manager = new CategoriaManager(new DB());
            LlenarTabla();
            DataContext = categoria;
            
        }

        private void BloquearBotones(bool v)
        {
            btnAgregar.IsEnabled = v;
            btnBorrar.IsEnabled = v;
            btnGuardar.IsEnabled = !v;
            btnEditar.IsEnabled = v;
            btnBuscar.IsEnabled = v;
            btnRestaurar.IsEnabled = v;
            btnCancelar.IsEnabled = !v;
        }

        private void LlenarTabla(List<Categoria> datos = null)
        {
            categoria = new Categoria();
            dtgCategorias.ItemsSource = null;
            if (datos != null)
            {
                dtgCategorias.ItemsSource = datos;
            }
            else
            {
                dtgCategorias.ItemsSource = manager.ObtenerTodas;
            }
            BloquearBotones(true);
            DataContext = categoria;
            if (manager.Error != "")
            {
                MessageBox.Show(manager.Error, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            esEdicion = false;
            categoria = new Categoria();
            BloquearBotones(false);
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            Categoria r;
            categoria = DataContext as Categoria;
            if (esEdicion)
            {
                //Actualizar
                r = manager.Actualizar(categoria);
            }
            else
            {
                //Insertar
                r = manager.Insertar(categoria);
            }
            if (r != null)
            {
                LlenarTabla();
            }
            else
            {
                MessageBox.Show(manager.Error, "Categorias", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            categoria = dtgCategorias.SelectedItem as Categoria;
            if (categoria == null)
            {
                MessageBox.Show("Primero seleccione una categoria", "Categorias", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                DataContext = categoria;
                esEdicion = true;
                BloquearBotones(false);
            }

        }

        private void btnBorrar_Click(object sender, RoutedEventArgs e)
        {
            categoria = dtgCategorias.SelectedItem as Categoria;
            if (categoria == null)
            {
                MessageBox.Show($"Primero seleccione una categoría", "Categorias", MessageBoxButton.OK, MessageBoxImage.Error);

            }
            else
            {
                if (MessageBox.Show($"¿Realmente desea eliminar la categoría {categoria.Nombre}?", "Categorias", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manager.Eliminar(categoria.IdCategoria))
                    {
                        LlenarTabla();
                    }
                    else
                    {
                        MessageBox.Show(manager.Error, "Categorias", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }

            }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            LlenarTabla(manager.BuscarCategoria(txbNombre.Text));
        }

        private void btnRestaurar_Click(object sender, RoutedEventArgs e)
        {
            LlenarTabla();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            LlenarTabla();
        }
    }
}
