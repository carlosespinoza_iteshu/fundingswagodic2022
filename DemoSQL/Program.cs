﻿using Microsoft.Data.SqlClient;

namespace DemoSQL
{
    internal class Program
    {
        static string cadenaDeConexion = @"Data Source=localhost;Initial Catalog=MiTiendita;Persist Security Info=True;User ID=sa;Password=Luxandra0;TrustServerCertificate=True";
        static SqlConnection conexion;
        static void Main(string[] args)
        {
            conexion = new SqlConnection(cadenaDeConexion);
            Console.Write("Categoria a insertar: ");
            string cat = Console.ReadLine();
            var antes = CantidadDeRegistrosEnTabla("Categorias");
            ComandoSQL($"Insert into Categorias(Nombre) values('{cat}')");
            var despues = CantidadDeRegistrosEnTabla("Categorias");
            if (antes < despues)
            {
                Console.WriteLine("Registro insertado");
            }
            else
            {
                Console.WriteLine("Registro NO insertado");
            }
            MostrarTablaCategorias();
            Console.ReadLine();
        }



        static long CantidadDeRegistrosEnTabla(string tabla)
        {
            string sql = $"Select Count(*) from {tabla}";
            try
            {
                conexion.Open();
                SqlCommand command = new SqlCommand(sql, conexion);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                long cantidad = reader.GetInt32(0);
                reader.Close();
                conexion.Close();
                return cantidad;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                return -1;
            }

        }

        static void MostrarTablaCategorias()
        {
            string sql = "select * from Categorias";
            try
            {
                conexion.Open();
                SqlCommand command = new SqlCommand(sql, conexion);
                SqlDataReader reader = command.ExecuteReader();
                Console.WriteLine("Id\tNombre");
                while (reader.Read())
                {
                    Console.WriteLine($"{reader.GetInt32(0)}\t{reader.GetString(1)}");
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

        static bool ComandoSQL(string sql)
        {
            conexion.Open();
            if(conexion.State== System.Data.ConnectionState.Open)
            {
                try
                {
                    SqlCommand comando = new SqlCommand(sql, conexion);
                    comando.ExecuteNonQuery();
                    conexion.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                    return false;
                }
            }
            else
            {
                Console.WriteLine("No se pudo abrir la conexión...");
                return false;
            }
        }
    }
}